import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationComponent } from '../components/pagination/pagination.component';
import { BreadcrumbsSearchComponent } from '../components/breadcrumbs-search/breadcrumbs-search.component';
import { CreatePostButtonComponent } from '../posts/create-post-button/create-post-button.component';
import { RouterModule } from '@angular/router';
import { SearchBoxComponent } from '../components/search-box/search-box.component';

@NgModule({
  declarations: [
    PaginationComponent,
    BreadcrumbsSearchComponent,
    CreatePostButtonComponent,
    SearchBoxComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationComponent,
    BreadcrumbsSearchComponent,
    CreatePostButtonComponent,
    SearchBoxComponent,
  ],
})
export class SharedModule { }
