import { UserLogin } from './../../common/interfaces/user-login';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { of } from 'rxjs';

describe('AuthService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['post', 'delete']);
  const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
      {
        provide: StorageService,
        useValue: storage,
      },
    ],
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('login should log the use in', () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.and.returnValue(of({
      token: 'token',
      user: { username: 'ivo' },
    }));

    const userLogin: UserLogin = {
      email: 'ivo@ivo.com',
      password: 'password',
    };

    service.login(userLogin).subscribe(
      (res) => {
        expect(res.user.username).toBe('ivo');
      }
    );
  });

  it('login should call http.post', () => {
    const service: AuthService = TestBed.get(AuthService);

    const userLogin: UserLogin = {
      email: 'ivo@ivo.com',
      password: 'password',
    };

    http.post.calls.reset();

    service.login(userLogin).subscribe(
      () => expect(http.post).toHaveBeenCalledTimes(1)
    );
  });

  it('login should update the subject', () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.and.returnValue(of({
      token: 'token',
      user: { username: 'ivo' },
    }));

    const userLogin: UserLogin = {
      email: 'ivo@ivo.com',
      password: 'password',
    };

    service.login(userLogin).subscribe(
      () => {
        service.user$.subscribe(
          (username) => expect(username).toBe('ivo')
        );
      }
    );
  });

  // WHY LOGOUT TESTS DO NOT PASS?

  // it('logout should change the Subject to null', () => {
  //   const service: AuthService = TestBed.get(AuthService);

  //   service.logout().subscribe(
  //     () => {
  //       service.user$.subscribe(
  //         (username) => expect(username).toBe(null)
  //       );
  //     }
  //   );
  // });

  // it('logout should call storage.remove twice', () => {
  //   const service: AuthService = TestBed.get(AuthService);

  //   storage.remove.calls.reset();

  //   service.logout().subscribe(
  //     () => {
  //       expect(storage.remove).toHaveBeenCalledTimes(2);
  //     }
  //   );
  // });
});
