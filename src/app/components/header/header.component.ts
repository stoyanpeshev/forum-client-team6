import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from './../../core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { Router } from '@angular/router';
import { UsersDataService } from '../../../app/users/services/users-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public isNavbarCollapsed = true;
  public userId: string;

  @Input() public loggedInUser: string;
  @Input() public isLogged: boolean;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly usersDataService: UsersDataService,
  ) { }

  ngOnInit() {
    this.authService.user$.subscribe(
      (user) => this.loggedInUser = user
    );
  }

  public onLogout(): void {
    this.authService.logout().subscribe(
      () => {
        this.notificator.success('Successful logout!');
        this.router.navigate(['/home']);
      },
      (error) => {
        this.notificator.error('Unsuccessful logout!');
      }
    );
  }

  public onName(): void {
    this.usersDataService.allUsers().subscribe(
      (users) => {

        users.forEach( (user) => {
          // console.log('iterate user name  ' +user.username);
          // console.log('iterate user id  ' +user.id);
          // console.log('this.loggedInUser   ='  +this.loggedInUser);
          if(user.username === this.loggedInUser) {
                this.userId = user.id;
                return;
          }});
        // console.log( users);
        // console.log('username   ' + uName );
        // console.log('userId  ' + this.userId);
        this.router.navigate(['/users', this.userId]);
      }

    );
  }

}
