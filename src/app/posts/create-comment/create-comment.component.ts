import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})
export class CreateCommentComponent implements OnInit {
  public commentForm: FormGroup;
  public currentContent: string;

  public options: Object = {
    placeholderText: 'Write your post here...',
    charCounterCount: true,
    heightMin: 170,
    heightMax: 500,
  };

  @Output() public createComment = new EventEmitter<string>();

  constructor(
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      comment: ['', [Validators.minLength(3), Validators.required]],
    });
  }

  public createCommentOfPost(): void {
    this.createComment.emit(this.commentForm.value.comment);
    this.commentForm.reset();
  }
}
