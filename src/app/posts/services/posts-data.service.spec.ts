import { CreatePost } from './../../common/interfaces/create-post';
import { TestBed } from '@angular/core/testing';

import { PostsDataService } from './posts-data.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('PostsDataService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
    ],
  }));

  it('should be created', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);
    expect(service).toBeTruthy();
  });

  it('allPosts should return all posts', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.and.returnValue(of([
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ]));

    service.allPosts().subscribe(
      (posts) => expect(posts[0].title).toBe('Post 1')
    );
  });

  it('allPosts should call http.get one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.calls.reset();

    service.allPosts().subscribe(
      (posts) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('allPostsCount should return the amount of all posts', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.and.returnValue(of(10));

    service.allPostsCount().subscribe(
      (postsCount) => expect(postsCount).toBe(10)
    );
  });

  it('allPostsCount should call http.get one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.calls.reset();

    service.allPostsCount().subscribe(
      (posts) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('searchPosts should return all found posts', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.and.returnValue(of([
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ]));

    service.searchPosts(1, 'post 1').subscribe(
      (posts) => expect(posts[0].title).toBe('Post 1')
    );
  });

  it('searchPosts should call http.get one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.calls.reset();

    service.searchPosts(1, 'post 1').subscribe(
      (posts) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('singlePost should return a single post', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.and.returnValue(of(
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ));

    service.singlePost('ab5035c0-f411-4868-b20e-e068ac88bbf9').subscribe(
      (post) => expect(post.title).toBe('Post 1')
    );
  });

  it('singlePost should call http.get one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.get.calls.reset();

    service.singlePost('ab5035c0-f411-4868-b20e-e068ac88bbf9').subscribe(
      (post) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('createPost should return the created post', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.post.and.returnValue(of(
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ));

    const postToCreate: CreatePost = {
      title: 'Post 1',
      content: 'Post Content',
    };

    service.createPost(postToCreate).subscribe(
      (post) => expect(post.title).toBe('Post 1')
    );
  });

  it('createPost should call http.post one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    const postToCreate: CreatePost = {
      title: 'Post 1',
      content: 'Post Content',
    };

    http.post.calls.reset();

    service.createPost(postToCreate).subscribe(
      (post) => expect(http.post).toHaveBeenCalledTimes(1)
    );
  });

  it('updatePost should return the updated post', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.put.and.returnValue(of(
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1 Updated',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ));

    const postToUpdate = {
      id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
      title: 'Post 1 Updated',
      content: 'Content 1',
      user: 'ivo',
      userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
      comments: [],
      postLikes: 0,
      postDislikes: 0,
      createdOn: new Date(),
      touchDateColumn: new Date(),
    };

    service.updatePost(postToUpdate).subscribe(
      (post) => expect(post.title).toBe('Post 1 Updated')
    );
  });

  it('updatePost should call http.put one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    const postToUpdate = {
      id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
      title: 'Post 1 Updated',
      content: 'Content 1',
      user: 'ivo',
      userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
      comments: [],
      postLikes: 0,
      postDislikes: 0,
      createdOn: new Date(),
      touchDateColumn: new Date(),
    };

    http.put.calls.reset();

    service.updatePost(postToUpdate).subscribe(
      (post) => expect(http.put).toHaveBeenCalledTimes(1)
    );
  });

  it('deletePost should return the deleted post', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.delete.and.returnValue(of(
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ));

    service.deletePost('ab5035c0-f411-4868-b20e-e068ac88bbf9').subscribe(
      (post) => expect(post.title).toBe('Post 1')
    );
  });

  it('deletePost should call http.delete one time', () => {
    const service: PostsDataService = TestBed.get(PostsDataService);

    http.delete.calls.reset();

    service.deletePost('ab5035c0-f411-4868-b20e-e068ac88bbf9').subscribe(
      (post) => expect(http.delete).toHaveBeenCalledTimes(1)
    );
  });
});
