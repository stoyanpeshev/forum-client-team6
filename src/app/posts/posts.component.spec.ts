import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsComponent } from './posts.component';
import { SharedModule } from '../shared/shared.module';
import { PostsDataService } from './services/posts-data.service';
import { NotificatorService } from '../core/services/notificator.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { PostViewComponent } from './post-view/post-view.component';
import { TimeAgoPipe } from 'time-ago-pipe';
import { LikesDislikesComponent } from './likes-dislikes/likes-dislikes.component';
import { EditDeleteComponent } from './edit-delete/edit-delete.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AuthService } from '../core/services/auth.service';
import { SearchService } from '../core/services/search.service';
import { Location } from '@angular/common';

import {} from 'jasmine';

describe('PostsComponent', () => {
  let component: PostsComponent;
  let fixture: ComponentFixture<PostsComponent>;

  const postsDataService = jasmine.createSpyObj('postsDataService', [
    'allPosts',
    'searchPosts',
  ]);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const authService = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register', 'loggedUser']);
  const searchService = jasmine.createSpyObj('SearchService', ['emitSearch']);
  const location = jasmine.createSpyObj('Location', ['go']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostsComponent,
        PostViewComponent,
        TimeAgoPipe,
        LikesDislikesComponent,
        EditDeleteComponent,
      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
      ],
      providers: [
        {
          provide: PostsDataService,
          useValue: postsDataService,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: AuthService,
          useValue: authService,
        },
        {
          provide: SearchService,
          useValue: searchService,
        },
        {
          provide: Location,
          useValue: location,
        },
      ],
    });
  }));

  beforeEach(() => {
    activatedRoute.data = of([
      {
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Post 1',
        content: 'Content 1',
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        comments: [],
        postLikes: 0,
        postDislikes: 0,
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ]);
    authService.user$ = of('test');
    activatedRoute.queryParamMap = of({
      params: {
        page: 1,
      },
    });
    searchService.search$ = of('test');

    fixture = TestBed.createComponent(PostsComponent);
    component = fixture.componentInstance;

    component.loggedInUserSubscription = authService.user$.subscribe();
    component.searchSubscription = searchService.search$.subscribe();

    fixture.detectChanges();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
