import { Post } from './../../common/interfaces/post';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PostsDataService } from './../services/posts-data.service';
import { Router } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  public createPostForm: FormGroup;

  public options: Object = {
    placeholderText: 'Write your post here...',
    charCounterCount: true,
    heightMin: 170,
    heightMax: 500,
  };

  constructor(
    private readonly postsDataService: PostsDataService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.createPostForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      content: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  public createPost(): void {
    console.log(this.createPostForm.value);
    this.postsDataService.createPost(this.createPostForm.value).subscribe(
      (post: Post) => {
        this.router.navigate(['/posts']);
        this.notificator.success('Post successfully created!');
      },
      (error) => {
        this.notificator.error('Post creation unsuccessful!');
      }
    );
  }

}
