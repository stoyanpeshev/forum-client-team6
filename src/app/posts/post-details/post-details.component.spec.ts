import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from '../../app.module';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AppRoutingModule } from '../../app-routing.module';
import { PostDetailsComponent } from './post-details.component';
import { NotificatorService } from '../../core/services/notificator.service';
import { PostsDataService } from '../services/posts-data.service';
import { PostsComponent } from '../posts.component';
import { PostViewComponent } from '../post-view/post-view.component';
import { TimeAgoPipe } from 'time-ago-pipe';
import { PostCommentsComponent } from '../post-comments/post-comments.component';
import { LikesDislikesComponent } from '../likes-dislikes/likes-dislikes.component';
import { CreateCommentComponent } from '../create-comment/create-comment.component';
import { CreatePostComponent } from '../create-post/create-post.component';
// import { CreatePostButtonComponent } from '../create-post-button/create-post-button.component';
import { UpdateCommentComponent } from '../update-comment/update-comment.component';
import { EditDeleteComponent } from '../edit-delete/edit-delete.component';
import { SharedModule } from '../../shared/shared.module';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { PostsRoutingModule } from '../posts-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('PostDetailsComponent', () => {
  let component: PostDetailsComponent;
  let fixture; // : ComponentFixture<PostDetailsComponent>;
  // ^^ fixture must be in each 'it' to test anew
  let notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  // let authenticator = jasmine.createSpyObj('AuthService', ['']);
  // let activatedRoute = jasmine.createSpyObj('ActivatedRoute', []);
  let postsDataService = jasmine.createSpyObj('PostsDataService', [
    'createCommentOfPost',
    'updatePostVote',
    'updateCommentVote',
    'updatePost',
    'deletePost',
    'updateComment',
    'deleteComment'
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        // added the declarations in the @NgModule metadata in posts.module.ts
        PostsComponent,
        PostViewComponent,
        TimeAgoPipe,
        PostDetailsComponent,
        PostCommentsComponent,
        LikesDislikesComponent,
        CreateCommentComponent,
        CreatePostComponent,
        //CreatePostButtonComponent,
        UpdateCommentComponent,
        EditDeleteComponent
      ],
      imports: [
        // added the imports in the @NgModule metadata in posts.module.ts
        AppModule,
        FroalaEditorModule,
        FroalaViewModule,
        SharedModule,
        RouterModule,
        PostsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        // from app.module
        AppRoutingModule,
        BrowserAnimationsModule,
        CoreModule,
        // solves the error: No provider for ActivatedRoute
        RouterTestingModule.withRoutes(
          [{path: '', component: PostDetailsComponent},
         // {path: 'simple', component: SimpleCmp}
        ]
        )
      ],
      providers: [
        // those mocked services in this test file
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: PostsDataService,
          useValue: postsDataService,
        },
      ],
    })
    .compileComponents();
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(PostDetailsComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the component', () => {
    fixture = TestBed.createComponent(PostDetailsComponent);

    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should call createCommentOfPost() one time with correct input', () => {
    fixture = TestBed.createComponent(PostDetailsComponent);
    component = fixture.debugElement.componentInstance;
    const event = 'click';

    var createCom, bar = null;

    createCom = {
      setBar: function(event) {
        bar = event;
      }
    };
    spyOn(createCom, 'setBar');
    createCom.setBar('click');


    expect(createCom.setBar).toHaveBeenCalledTimes(1);
    expect(createCom.setBar).toHaveBeenCalledWith(event);
  });

});
