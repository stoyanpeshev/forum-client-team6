import { PostsDataService } from './../services/posts-data.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from './../../common/interfaces/post';
import { CommentInterface } from '../../common/interfaces/show-comment';
import { NotificatorService } from '../../core/services/notificator.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { PostsCommentsBaseComponent } from '../posts-comments--base.component';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent extends PostsCommentsBaseComponent implements OnInit, OnDestroy {
  public post: Post;
  public comments: CommentInterface[];
  public loggedInUserSubscription: Subscription;
  public loggedInUser: string;

  constructor(
    postsDataService: PostsDataService,
    notificator: NotificatorService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {
    super(postsDataService, notificator);
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.post = data.post;
      this.comments = this.post.comments;
    });

    this.loggedInUserSubscription = this.authService.user$.subscribe(
      (user) => {
        this.loggedInUser = user;
      }
    );
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
  }

  public createCommentOfPost(event: string): void {
    const createCommentOfPost = {
      message: event,
    };

    this.postsDataService.createCommentOfPost(this.post.id, createCommentOfPost).subscribe(
      (comment: CommentInterface) => {
        this.comments.push(comment);
        this.notificator.success('Thank you for your comment!');
      },
      (error) => {
        this.notificator.error('Unsuccessful comment creation!');
      }
    );
  }

  public itemLike(postId: string): void {
    this.postsDataService.updatePostVote(postId, true).subscribe(
      (post: Post) => {
        this.post.postLikes = post.postLikes;
        this.post.postDislikes = post.postDislikes;

        this.notificator.success('You liked the post successfully!');
      },
      (error) => {
        if (error.error.message === 'User has already liked this post.') {
          this.notificator.error('You have already liked this post.');
        }
      }
    );
  }

  public itemDislike(postId: string): void {
    this.postsDataService.updatePostVote(postId, false).subscribe(
      (post: Post) => {
        this.post.postLikes = post.postLikes;
        this.post.postDislikes = post.postDislikes;

        this.notificator.success('You disliked the post successfully!');
      },
      (error) => {
        if (error.error.message === 'User has already disliked this post.') {
          this.notificator.error('You have already disliked this post.');
        }
      }
    );
  }

  public updateItem(item: Post): void {
    this.postsDataService.updatePost(item).subscribe(
      (post: Post) => {
        this.notificator.success('Post updated successfully!');
      },
      (error) => {
        this.notificator.error('Post update failed!');
      }
    );
  }

  public deleteItem(postId: string): void {
    this.postsDataService.deletePost(postId).subscribe(
      (post: Post) => {
        this.router.navigate(['/posts']);
        this.notificator.success('Post deteled successfully!');
      },
      (error) => {
        this.notificator.error('Post delete failed!');
      }
    );
  }
}
