import { PostsRoutingModule } from './posts-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PostsComponent } from './posts.component';
import { PostViewComponent } from './post-view/post-view.component';
import { TimeAgoPipe } from 'time-ago-pipe';
import { PostDetailsComponent } from './post-details/post-details.component';
import { RouterModule } from '@angular/router';
import { PostCommentsComponent } from './post-comments/post-comments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateCommentComponent } from './create-comment/create-comment.component';
import { LikesDislikesComponent } from './likes-dislikes/likes-dislikes.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { UpdateCommentComponent } from './update-comment/update-comment.component';
import { EditDeleteComponent } from './edit-delete/edit-delete.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  declarations: [
    PostsComponent,
    PostViewComponent,
    TimeAgoPipe,
    PostDetailsComponent,
    PostCommentsComponent,
    LikesDislikesComponent,
    CreateCommentComponent,
    CreatePostComponent,
    UpdateCommentComponent,
    EditDeleteComponent,
  ],
  imports: [
    SharedModule,
    RouterModule,
    PostsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
  ],
  exports: [
    PostViewComponent,
    PostCommentsComponent,
  ]
})
export class PostsModule { }
