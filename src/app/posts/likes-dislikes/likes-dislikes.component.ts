import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-likes-dislikes',
  templateUrl: './likes-dislikes.component.html',
  styleUrls: ['./likes-dislikes.component.css']
})
export class LikesDislikesComponent implements OnInit {
  @Input() public itemLikes: number;
  @Input() public itemDislikes: number;

  @Output() public liked = new EventEmitter();
  @Output() public disliked = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public itemLike(): void {
    this.liked.emit();
  }

  public itemDislike(): void {
    this.disliked.emit();
  }

}
