import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentInterface } from './../../common/interfaces/show-comment';
import { Post } from './../../common/interfaces/post';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-delete',
  templateUrl: './edit-delete.component.html',
  styleUrls: ['./edit-delete.component.css']
})
export class EditDeleteComponent implements OnInit {
  @Input() public applyTo: string;
  @Input() public item: Post & CommentInterface;
  public postEditForm: FormGroup;
  public commentEditForm: FormGroup;

  public options: Object = {
    placeholderText: 'Write your post here...',
    charCounterCount: true,
    heightMin: 170,
    heightMax: 500,
  };

  @Output() public updatedItem = new EventEmitter<Post | CommentInterface>();
  @Output() public deletedItem = new EventEmitter();

  constructor(
    private readonly modalService: NgbModal,
    private readonly formbuilder: FormBuilder,
  ) { }

  ngOnInit() {
    if (this.applyTo === 'posts') {
      this.postEditForm = this.formbuilder.group({
        title: ['', [Validators.required, Validators.minLength(3)]],
        content: ['', [Validators.required, Validators.minLength(3)]],
      });

      this.postEditForm.controls.title.setValue(this.item.title);
      this.postEditForm.controls.content.setValue(this.item.content);
    } else if (this.applyTo === 'comments') {
      this.commentEditForm = this.formbuilder.group({
        message: ['', [Validators.minLength(3), Validators.required]],
      });

      this.commentEditForm.controls.message.setValue(this.item.message);
    }
  }

  public open(modalWindow): void {
    this.modalService.open(modalWindow);
  }

  public updateItem(): void {
    if (this.applyTo === 'posts') {
      this.item.title = this.postEditForm.value.title;
      this.item.content = this.postEditForm.value.content;
    } else if (this.applyTo === 'comments') {
      this.item.message = this.commentEditForm.value.message;
    }

    this.updatedItem.emit(this.item);
  }

  public deleteItem(): void {
    this.deletedItem.emit();
  }

}
