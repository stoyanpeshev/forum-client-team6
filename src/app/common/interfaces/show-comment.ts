export interface CommentInterface {
  id: string;
  message: string;
  user: string;
  userID: string;
  commentLikes: number;
  commentDislikes: number;
  createdOn: Date;
}
