import { CommentInterface } from './show-comment';

export interface Post {
  id: string;
  title: string;
  content: string;
  user: string;
  userID: string;
  comments?: CommentInterface[];
  postLikes: number;
  postDislikes: number;
  createdOn: Date;
  touchDateColumn: Date;
}
