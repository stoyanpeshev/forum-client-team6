import { AppRoutingModule } from './app-routing.module';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthService } from './core/services/auth.service';
import { NotificatorService } from './core/services/notificator.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const authService = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register', 'loggedUser']);
  const http = jasmine.createSpyObj('HttpClient', ['post', 'delete']);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        HeaderComponent,
        LoginComponent,
        RegisterComponent,
        NotFoundComponent,
        FooterComponent,
      ],
      imports: [
        SharedModule,
        AppRoutingModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: authService,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: HttpClient,
          useValue: http,
        },
      ]
    });
  }));

  beforeEach(() => {
    authService.user$ = of('test');

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    component.loggedInSubscription = authService.user$.subscribe();

    fixture.detectChanges();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with the correct logged user data', async () => {
    expect(component.loggedInUser).toBe('test');
  });
});
