import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import {} from 'jasmine';
import { UserProfileComponent } from './user-profile.component';
import { UsersComponent } from '../users.component';
import { UsersViewComponent } from '../user-view/users-view.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, ActivatedRoute } from '@angular/router';
import { UsersRoutingModule } from '../users-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostsModule } from 'src/app/posts/posts.module';
import { PostsDataService } from 'src/app/posts/services/posts-data.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UsersDataService } from '../services/users-data.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;

  const postsDataService = jasmine.createSpyObj('PostsDataService', [
    'createCommentOfPost',
    'updatePostVote',
    'updateCommentVote',
    'updatePost',
    'deletePost',
    'updateComment',
    'deleteComment'
  ]);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['data']);
  const authService = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register', 'loggedUser']);
  const http = jasmine.createSpyObj('HttpClient', ['get','put','delete','post']);
  const usersDataService = jasmine.createSpyObj('UsersDataService', [
      'allUsers',
      'singleUser',
      'postsOfUser',
      'commentsOfUser',
      'userFriends',
      'addFriend',
      'removeFriend'
    ]);
  

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UsersComponent,
        UsersViewComponent,
        UserProfileComponent,
      ],
      imports: [
        SharedModule,
        RouterModule,
        UsersRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        PostsModule,
        RouterTestingModule.withRoutes(
            [
                {path: 'userId123', component: UserProfileComponent},
            ]),],
      providers: [
        {
          provide: PostsDataService,
          useValue: postsDataService,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: AuthService,
          useValue: authService,
        },
        {
            provide: HttpClient,
            useValue: http
        },
        {
            provide: UsersDataService,
            useValue: usersDataService
        },
      ],
    });
  }));

  beforeEach(() => {
    activatedRoute.data = of({
      user: {
        id: 'userId123',
        username: 'testusername',
        email: 'testemail',
        firstName: 'testfirstname',
        lastName: 'lastname',
        friends: [{
            id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
            username: 'testusername1',
            email: 'testemail1',
            firstName: 'testfirstname1',
            lastName: 'lastname1',
            friends: [],
            createdOn: new Date(),
          },
        ],
        createdOn: new Date(),
      },
    });
    authService.user$ = of('test');
    usersDataService.postsOfUser = of([{
        id: 'postId',
        title: 'posttest',
        content: 'contesttest',
        user: 'testusername',
        userID: 'userId123',
        comments: [],//not ready
        postLikes: 10,
        postDislikes: 2,
        createdOn: new Date(),
        touchDateColumn: new Date()
    }]);

    usersDataService.commentsOfUser = of([
        {
            id: 'commentId',
            message: 'messagetest',
            user: 'testusername',
            userID: 'userId123',
            commentLikes: 30,
            commentDislikes: 18,
            createdOn: new Date(),
        },
    ]);

    usersDataService.userFriends = of([
        {
            id: '22222',
            username: 'testusername2',
            email: 'testemail2',
            firstName: 'testfirstname2',
            lastName: 'lastname2',
            friends: [],
            createdOn: new Date(),
        }
    ]);

    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;

    component.loggedInUserSubscription = authService.user$.subscribe();

    fixture.detectChanges();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
