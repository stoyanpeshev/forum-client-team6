
import { of } from 'rxjs';
import { User } from '../../common/interfaces/user';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { UsersDataService } from './users-data.service';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User[] | {users: User[]}> {

  constructor(
    private readonly usersDataService: UsersDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {  

   // this.usersDataService.allUsers().subscribe((data)=>console.log(data));

    return this.usersDataService.allUsers()
        .pipe(catchError(
          (res) => {
            this.notificator.error(res.error.error);
            console.log("We have error at user resolver service.");
            return of({users: null});
          }
        ));
  }
}
