import { TestBed } from "@angular/core/testing";
import { HttpClient } from '@angular/common/http';
import { UsersDataService } from './users-data.service';
import { of } from 'rxjs';

describe('UsersDataService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  
    beforeEach(() => TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    }));
  
    it('should be created', () => {
      const service: UsersDataService = TestBed.get(UsersDataService);
      expect(service).toBeTruthy();
    });

    it('allUsers should return all users', () => {
        const service: UsersDataService = TestBed.get(UsersDataService);
    
        http.get.and.returnValue(of([
            {
                id: 'userId123',
                username: 'testusername',
                email: 'testemail',
                firstName: 'testfirstname',
                lastName: 'lastname',
                friends: [{
                    id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
                    username: 'testusername1',
                    email: 'testemail1',
                    firstName: 'testfirstname1',
                    lastName: 'lastname1',
                    friends: [],
                    createdOn: new Date(),
                  },
                ],
                createdOn: new Date(),
              },,
        ]));
    
        service.allUsers().subscribe(
          (users) => expect(users[0].id).toBe('userId123')
        );
      });

      it('allUsers should call http.get one time', () => {
        const service: UsersDataService = TestBed.get(UsersDataService);
    
        http.get.calls.reset();
    
        service.allUsers().subscribe(
          (users) => expect(http.get).toHaveBeenCalledTimes(1)
        );
      });

      it('allUsers should return all users', () => {
        const service: UsersDataService = TestBed.get(UsersDataService);
    
        http.get.and.returnValue(of(
            {
                id: 'userId123',
                username: 'testusername',
                email: 'testemail',
                firstName: 'testfirstname',
                lastName: 'lastname',
                friends: [{
                    id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
                    username: 'testusername1',
                    email: 'testemail1',
                    firstName: 'testfirstname1',
                    lastName: 'lastname1',
                    friends: [],
                    createdOn: new Date(),
                  },
                ],
                createdOn: new Date(),
              },
        ));
    
        service.singleUser('userId123').subscribe(
          (users) => expect(users.id).toBe('userId123')
        );
      });

      it('singleUser should call http.get one time', () => {
        const service: UsersDataService = TestBed.get(UsersDataService);
    
        http.get.calls.reset();
    
        service.singleUser('someId').subscribe(
          (user) => expect(http.get).toHaveBeenCalledTimes(1)
        );
      });

      it('allUsers should return all users', () => {
        const service: UsersDataService = TestBed.get(UsersDataService);
    
        http.post.and.returnValue(of(
            {
                id: 'userId123',
                username: 'testusername',
                email: 'testemail',
                firstName: 'testfirstname',
                lastName: 'lastname',
                friends: [{
                    id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
                    username: 'testusername1',
                    email: 'testemail1',
                    firstName: 'testfirstname1',
                    lastName: 'lastname1',
                    friends: [],
                    createdOn: new Date(),
                  },
                ],
                createdOn: new Date(),
              },
        ));
    
        service.addFriend('userId123').subscribe(
          (users) => expect(users.id).toBe('userId123')
        );
      });
});