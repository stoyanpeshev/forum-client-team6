import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../common/interfaces/user';
import { Post } from '../../common/interfaces/post';
import { CommentInterface } from '../../common/interfaces/show-comment';

@Injectable({
    providedIn: 'root'
})
export class UsersDataService {
    constructor(
        private readonly http: HttpClient,
    ) { }

    public allUsers(): Observable<User[]> {
        return this.http.get<User[]>('http://localhost:3000/api/users');
    }

    public singleUser(userId: string): Observable<User> {
        return this.http.get<User>(`http://localhost:3000/api/users/${userId}`);
    }

    public postsOfUser(username: string): Observable<Post[]> {
        return this.http.get<Post[]>(`http://localhost:3000/api/posts?user=${username}`);
    }

    public commentsOfUser(userId: string): Observable<CommentInterface[]> {
        return this.http.get<CommentInterface[]>(`http://localhost:3000/api/posts/comments/${userId}`);
    }

    // http://localhost:3000/api/users/1b54efda-ceba-4ea9-92a7-9b7feaacd7b5/friends
    public userFriends(userId: string): Observable<User[]> {
        return this.http.get<User[]>(`http://localhost:3000/api/users/${userId}/friends`);
    }

    public addFriend(targetUserId: string): Observable<User> {
        return this.http.post<User>(`http://localhost:3000/api/users/${targetUserId}/friends`,{});
    }

    public removeFriend(targetUserId: string): Observable<User> {
        return this.http.delete<User>(`http://localhost:3000/api/users/${targetUserId}/friends`,{});
    }
}

